from PseudoJava import *
import enum
import g as g


class Becko:
    def __init__(self):
        self._a: int = 0
        self._b: int = 0
        self._c: int = 0
        self._d: int = 0
        self._e: int = 0

    def a(self) -> int:
        return self._b

    def b(self) -> int:
        return self._a

    def c(self) -> int:
        return self._d

    def d(self) -> int:
        return self._c

    def e(self, var1: int) -> None:
        self._e = var1

    def f(self, var1: int) -> None:
        self._b = var1

    def g(self, var1: int) -> None:
        self._a = var1

    def h(self, var1: int) -> None:
        self._d = var1

    def i(self, var1: int) -> None:
        self._c = var1


class Cecko:
    def __init__(self):
        self._a: int = 0
        self._b: int = 0
        self._c: int = 0
        self._d: int = 0
        self._e: list = []

    def a(self) -> int:
        return self._b

    def b(self) -> int:
        return self._a

    def c(self) -> list:
        return self._e

    def d(self) -> int:
        return self._c

    def e(self) -> int:
        return self._d

    def f(self, var1: int) -> None:
        self._b = var1

    def g(self, var1: int) -> None:
        self._a = var1

    def h(self, var1: int) -> None:
        self._c = var1

    def i(self, var1: int) -> None:
        self._d = var1


class Acko:
    def __init__(self):
        self._a: int = 0
        self._b: int = 0
        self._c: int = 0
        self._d: Cecko = None

    def a(self) -> int:
        return self._b

    def b(self) -> int:
        return self._a

    def c(self) -> Cecko:
        return self._d

    def d(self, var1: int) -> None:
        self._b = var1

    def e(self, var1: int) -> None:
        self._a = var1

    def f(self, var1: Cecko) -> None:
        self._d = var1

    def g(self, var1: int) -> None:
        self._c = var1


class Decko:
    a = [0 for i in range(256)]

    # celá tadle divná hovadinka s nějakou tajemnou nudlí pseudo mp2t packetů nemá žádnej smysl jenom se vezme dýlka toho pole
    # v několika zřetězenejch funkcích ruzně se to podělí/pronásobí/etc a vysledná hodnota se použije jako dýlka pole skovanýho
    # v tý statický proměný 'a' a vyplní se to nulama
    # mužem to spočitat manualně a dojdem k použitý hodnotě '256'

    """
    @staticvariable
    a = d.decode_I("00000000b71dc1046e3b8209d926430ddc7604136b6bc517b24d861a0550471eb8ed08260ff0c922d6d68a2f61cb4b2b649b0c35d386cd310aa08e3cbdbd4f3870db114cc7c6d0481ee09345a9fd5241acad155f1bb0d45bc2969756758b5652c836196a7f2bd86ea60d9b6311105a6714401d79a35ddc7d7a7b9f70cd665e74e0b6239857abe29c8e8da191399060953cc0278b8bdde68f52fba582e5e66486585b2bbeef46eaba3660a9b7817d68b3842d2fad3330eea9ea16ada45d0b6ca0906d32d42770f3d0fe56b0dd494b71d94c1b36c7fb06f7c32220b4ce953d75ca28803af29f9dfbf646bbb8fbf1a679fff4f63ee143ebffe59acdbce82dd07dec77708634c06d4730194b043dae56c539ab0682271c1b4323c53d002e7220c12acf9d8e1278804f16a1a60c1b16bbcd1f13eb8a01a4f64b057dd00808cacdc90c07ab9778b0b6567c69901571de8dd475dbdd936b6cc0526fb5e6116202fbd066bf469f5e085b5e5ad17d1d576660dc5363309b4dd42d5a490d0b1944ba16d84097c6a5ac20db64a8f9fd27a54ee0e6a14bb0a1bffcad60bb258b23b69296e2b22f2bad8a98366c8e41102f83f60dee87f35da9994440689d9d662b902a7bea94e71db4e0500075e4892636e93e3bf7ed3b6bb0f38c7671f7555032fae24df3fe5ff0bcc6e8ed7dc231cb3ecf86d6ffcb8386b8d5349b79d1edbd3adc5aa0fbd8eee00c6959fdcd6d80db8e6037c64f643296087a858bc97e5cad8a73ebb04b77560d044fe110c54b383686468f2b47428a7b005c3d66c158e4408255535d43519e3b1d252926dc21f0009f2c471d5e28424d1936f550d8322c769b3f9b6b5a3b26d6150391cbd40748ed970afff0560efaa011104dbdd014949b93192386521d0e562ff1b94beef5606dadf8d7706cfcd2202be2653deae6bc1ba9eb0b0668efb6bb27d701a6e6d3d880a5de6f9d64da6acd23c4ddd0e2c004f6a1cdb3eb60c97e8d3ebdc990ffb910b6bcb4a7ab7db0a2fb3aae15e6fbaaccc0b8a77bdd79a3c660369b717df79fa85bb4921f4675961a163288ad0bf38c742db081c330718599908a5d2e8d4b59f7ab085440b6c95045e68e4ef2fb4f4a2bdd0c479cc0cd43217d827b9660437f4f460072f85bc176fd0b86684a16476c93300461242dc565e94b9b115e565a1587701918306dd81c353d9f0282205e065b061d0bec1bdc0f51a69337e6bb52333f9d113e8880d03a8dd097243acd5620e3eb152d54f6d4297926a9c5ce3b68c1171d2bcca000eac8a550add6124d6cd2cb6b2fdf7c76eedbc1cba1e376d660e7aff023ea18ede2ee1dbda5f0aaa064f4738627f9c49be6fd09fdb889bee0798d67c63a80d0dbfb84d58bbc9a62967d9ebbb03e930cadff97b110b0af060d71abdf2b32a66836f3a26d66b4bcda7b75b8035d36b5b440f7b1");

    
    @staticmethod
    def decode_I(src: str) -> list:
        #_d = d.decode_B(src)
        # víme že to dycky krmíme samejma nulama
        return [0 * (len(src)*4)]
        


    @staticmethod
    def decode_B(src: str) -> bytes:
        return bytes(len(src) // 2)
    """


class Codec(enum.Enum):
    # inkrementovaný vo jedničku voproti javě protože jinak to nefunguje
    # (nejspiš bug/nedostatek dekompilatoru)
    MUXTS_CODEC_HEVC = 1
    MUXTS_CODEC_AVC = 2
    MUXTS_CODEC_AAC = 3
    MUXTS_CODEC_PCMA = 4


class Paketovadlo:

    """
    private final byte[] a = new byte[188];
    private int b = 0;
    private ByteBuffer c = ByteBuffer.allocateDirect(512000);
    private final h0.a d;
    private int e;
    private int f;
    private int g;
    private final List<h0.g.e> h;
    private final List<h> i;
    private boolean j;
    """

    def __init__(self):
        var1 = Acko()
        var1.e(0)
        var1.d(0)
        var1.g(0)
        var1.f(Cecko())

        self._a = [0 for i in range(188)]
        self._b = 0
        self._c = [0 for i in range(512000)]  # puvodně byteBufer
        self._d = var1
        self._g = 0
        self._h = []
        self._i = []
        self._j = False

    def c(self, var1: int, var2: int, var3: int) -> int:
        return jakoby_byte_overflow(var1 << var2 | var3)

    def d(self, var1: int, var2: int, var3: int) -> int:
        return jakoby_int_overflow(var1 << var2 | var3)

    def e(self, var1: int, var2: int, var3: int) -> int:
        return jakoby_long_overflow(var1 << var2 | var3)

    def f(self, var1: int) -> list:
        return [
            jakoby_byte_overflow(var1 >> 24 & 255),
            jakoby_byte_overflow(var1 >> 16 & 255),
            jakoby_byte_overflow(var1 >> 8 & 255),
            jakoby_byte_overflow(var1 & 255),
        ]

    def g(self, var1: int) -> list:
        return [
            jakoby_byte_overflow(jakoby_int_overflow(var1 >> 56 & 255)),
            jakoby_byte_overflow(jakoby_int_overflow(var1 >> 48 & 255)),
            jakoby_byte_overflow(jakoby_int_overflow(var1 >> 40 & 255)),
            jakoby_byte_overflow(jakoby_int_overflow(var1 >> 32 & 255)),
            jakoby_byte_overflow(jakoby_int_overflow(var1 >> 24 & 255)),
            jakoby_byte_overflow(jakoby_int_overflow(var1 >> 16 & 255)),
            jakoby_byte_overflow(jakoby_int_overflow(var1 >> 8 & 255)),
            jakoby_byte_overflow(jakoby_int_overflow(var1 & 255)),
        ]

    def h(self, var1: int) -> int:
        return var1 + 1 & 15

    def i(self, var1: list, var2: int, var3: int) -> int:
        # var4 = 255
        var4 = -1
        for var5 in range(var2, var2 + var3):
            var6 = var1[var5]
            var4 = Decko.a[(var4 >> 24 ^ var6) & 255] ^ var4 << 8

        return var4

    def j(self, var1: int) -> int:
        if self._b + var1 > 188:
            return -4
        else:
            for i in range(var1):
                # self._a[self._b + i] = 255
                self._a[self._b + i] = -1  # pozor!!
            self._b += var1
            return 0

    def o(self, var1: int) -> None:
        _i = self.i(self._a, var1, self._b - var1)
        _f = self.f(_i)
        jakobyArrayCopy(_f, 0, self._a, self._b, 4)
        self._b += 4

    def p(self) -> None:
        if self._b == 188:
            self._c += self._a[:188]  # možná blbost
            # for _ in self

            self._b = 0

    # var3 == g.Cecko
    def q(self, var1: int, var2: bool, var3: object) -> None:
        var4 = g.Acko()
        var4.l(var1)
        var4.m(0)
        var4.n(0)

        if var2:
            var4.q(255)
            var4.r(0)
            var4.p(var3)
        else:
            var4.q(0)
            var4.r(0)

        if 1 == var1:
            var4.r(0)

        var4.o(0)
        var4.s(0)
        var4.t(0)
        var4.k(0)

        var5 = self.c(0, 0, var4.b())
        var8 = self._a
        var1 = self._b
        var8[var1] = var5
        self._b = var1 + 1

        if var4.b() != 0:
            var5 = self.c(
                self.c(
                    self.c(
                        self.c(
                            self.c(
                                self.c(
                                    self.c(self.c(0, 0, var4.c()), 1, var4.h()),
                                    1,
                                    var4.d(),
                                ),
                                1,
                                var4.g(),
                            ),
                            1,
                            var4.e(),
                        ),
                        1,
                        var4.i(),
                    ),
                    1,
                    var4.j(),
                ),
                1,
                var4.a(),
            )
            var8 = self._a
            var1 = self._b
            var8[var1] = var5
            self._b = var1 + 1

            if var2:
                var6 = 0
                var5 = jakoby_byte_overflow(
                    jakoby_int_overflow(var4.f().a() >> 25 & 255 | var6)
                )
                var8 = self._a
                var1 = self._b
                var8[var1] = var5
                self._b = var1 + 1
                jakobyArrayCopy(
                    self.f(
                        self.d((var6 + (var4.f().a() & 33554431)), 6, var4.f().c()) << 1
                        | var4.f().b() & 256
                    ),
                    0,
                    self._a,
                    self._b,
                    4,
                )
                self._b += 4
                var5 = var4.f().b() | 0
                var8 = self._a
                var1 = self._b
                var8[var1] = var5
                self._b = var1 + 1
            else:
                self.j(var4.b() - 1)

    def s(self, var1: int, var2: int, var3: int, var4: int) -> None:
        var5 = g.Becko()
        var5.m(71)
        var5.n(0)
        var5.l(var2)
        var5.o(0)
        var5.k(var1)
        var5.p(0)
        var5.i(var3)
        var5.j(var4)
        jakobyArrayCopy(
            self.f(
                self.d(
                    self.d(
                        self.d(
                            self.d(
                                self.d(
                                    self.d(
                                        self.d(self.d(0, 0, var5.e()), 1, var5.f()),
                                        1,
                                        var5.d(),
                                    ),
                                    1,
                                    var5.g(),
                                ),
                                13,
                                var5.c(),
                            ),
                            2,
                            var5.h(),
                        ),
                        2,
                        var5.a(),
                    ),
                    4,
                    var5.b(),
                )
            ),
            0,
            self._a,
            self._b,
            4,
        )
        self._b = 4
        if 1 == var2 and 1 == var3:
            self._a[4] = 0
            self._b = 4 + 1

    def t(self, var1: list, var2: bool) -> None:
        if var1 is not None and len(var1) > 0:
            var3 = g.Decko()
            var3.u(0)
            var3.t(255)
            var3.x(0)
            var3.p(255)

            var4 = 0
            var5 = 0
            if var2:
                var4 = 9
                var5 = 5
            else:
                var4 = 8
                var5 = 4

            var3.r(var4 + len(var1) * 4)
            var3.v(1)
            var3.q(255)
            var3.w(0)
            var3.m(255)
            var3.s(0)
            var3.n(0)
            var3.o(var1)
            var3.l(1)
            jakobyArrayCopy(
                self.g(
                    self.e(
                        self.e(
                            self.e(
                                self.e(
                                    self.e(
                                        self.e(
                                            self.e(
                                                self.e(
                                                    self.e(
                                                        self.e(
                                                            self.e(0, 0, var3.h()),
                                                            1,
                                                            var3.g(),
                                                        ),
                                                        1,
                                                        var3.k(),
                                                    ),
                                                    2,
                                                    var3.c(),
                                                ),
                                                12,
                                                var3.e(),
                                            ),
                                            16,
                                            var3.i(),
                                        ),
                                        2,
                                        var3.d(),
                                    ),
                                    5,
                                    var3.j(),
                                ),
                                1,
                                var3.a(),
                            ),
                            8,
                            var3.f(),
                        ),
                        8,
                        var3.b(),
                    )
                ),
                0,
                self._a,
                self._b,
                8,
            )
            self._b += 8

            for cosi in var1:
                jakobyArrayCopy(
                    self.f(
                        self.d(
                            self.d(self.d(0, 0, cosi.b()), 3, cosi.c()), 13, cosi.a()
                        )
                    ),
                    0,
                    self._a,
                    self._b,
                    4,
                )
                self._b += 4

            self.o(var5)
            self.j(188 - self._b)
            self.p()

    # var5 == g.Icko
    def u(self, var1: int, var2: int, var3: int, var4: int, var5: object) -> None:
        var6 = g.Efko()
        var6.z(1)
        var6.H(var2)
        var6.D(var1)
        var6.x(2)
        var6.F(0)
        var6.E(0)
        var6.u(0)
        var6.s(0)
        var6.y(0)
        var6.G(var3)
        var6.w(0)
        var6.v(0)
        var6.t(0)
        var6.r(0)
        var6.A(0)
        var6.B(0)
        var6.C(var4)
        jakobyArrayCopy(
            self.g(
                self.e(
                    self.e(
                        self.e(
                            self.e(
                                self.e(
                                    self.e(
                                        self.e(
                                            self.e(
                                                self.e(
                                                    self.e(
                                                        self.e(
                                                            self.e(
                                                                self.e(
                                                                    self.e(
                                                                        self.e(
                                                                            self.e(
                                                                                0,
                                                                                0,
                                                                                var6.i(),
                                                                            ),
                                                                            8,
                                                                            var6.q(),
                                                                        ),
                                                                        16,
                                                                        var6.m(),
                                                                    ),
                                                                    2,
                                                                    var6.g(),
                                                                ),
                                                                2,
                                                                var6.o(),
                                                            ),
                                                            1,
                                                            var6.n(),
                                                        ),
                                                        1,
                                                        var6.d(),
                                                    ),
                                                    1,
                                                    var6.b(),
                                                ),
                                                1,
                                                var6.h(),
                                            ),
                                            2,
                                            var6.p(),
                                        ),
                                        1,
                                        var6.f(),
                                    ),
                                    1,
                                    var6.e(),
                                ),
                                1,
                                var6.c(),
                            ),
                            1,
                            var6.a(),
                        ),
                        1,
                        var6.j(),
                    ),
                    1,
                    var6.k(),
                )
            ),
            0,
            self._a,
            self._b,
            8,
        )
        self._b += 8
        var7 = jakoby_byte_overflow(var6.l() | 0)
        var8 = self._a
        var1 = self._b
        var8[var1] = var7
        self._b = var1 + 1
        if 2 == var6.p() and 5 == var6.l():
            var7 = self.c(self.c(self.c(0, 0, var5.a()), 3, var5.b()), 1, var5.e())
            var8 = self._a
            var1 = self._b
            var8[var1] = var7
            self._b = var1 + 1
            jakobyArrayCopy(
                self.f(
                    self.d(
                        self.d(
                            self.d(self.d(0, 0, var5.c()), 1, var5.f()), 15, var5.d()
                        ),
                        1,
                        var5.g(),
                    )
                ),
                0,
                self._a,
                self._b,
                4,
            )
            self._b += 4

        if 3 == var6.p() and 10 == var6.l():
            var5.h(3)
            var7 = self.c(self.c(self.c(0, 0, var5.a()), 3, var5.b()), 1, var5.e())
            var10 = self._a
            var1 = self._b
            var10[var1] = var7
            self._b = var1 + 1
            jakobyArrayCopy(
                self.f(
                    self.d(
                        self.d(
                            self.d(self.d(0, 0, var5.c()), 1, var5.f()), 15, var5.d()
                        ),
                        1,
                        var5.g(),
                    )
                ),
                0,
                self._a,
                self._b,
                4,
            )
            self._b += 4

        if 224 == var2 and 27 == self._d.c().c()[self._f].d():
            var9 = self._a
            var9[self._b] = 0
            self._b += 1
            var9[self._b] = 0
            self._b += 1
            var9[self._b] = 0
            self._b += 1
            var9[self._b] = 1
            self._b += 1
            var9[self._b] = 9
            self._b += 1
            var9[self._b] = -16
            self._b += 1

    def v(self, var1: list, var2: int, var3: bool) -> None:
        if var1 is not None and len(var1) > 0:
            var4 = g.Gecko()
            var4.D(2)
            var4.C(1)
            var4.F(0)
            var4.w(255)
            var5 = 0
            var6 = 0
            if var3:
                var5 = 13
                var6 = 5
            else:
                var5 = 12
                var6 = 4

            var4.A(var5 + len(var1) * 5)
            var4.v(var2)
            var4.x(255)
            var4.E(0)
            var4.q(255)
            var4.B(0)
            var4.r(0)
            var4.y(255)
            var4.s(256)
            var4.z(255)
            var4.u(0)
            var4.t(var1)
            var4.p(1)
            jakobyArrayCopy(
                self.g(
                    self.e(
                        self.e(
                            self.e(
                                self.e(
                                    self.e(
                                        self.e(
                                            self.e(
                                                self.e(
                                                    self.e(
                                                        self.e(
                                                            self.e(0, 0, var4.m()),
                                                            1,
                                                            var4.l(),
                                                        ),
                                                        1,
                                                        var4.o(),
                                                    ),
                                                    2,
                                                    var4.f(),
                                                ),
                                                12,
                                                var4.j(),
                                            ),
                                            16,
                                            var4.e(),
                                        ),
                                        2,
                                        var4.g(),
                                    ),
                                    5,
                                    var4.n(),
                                ),
                                1,
                                var4.a(),
                            ),
                            8,
                            var4.k(),
                        ),
                        8,
                        var4.b(),
                    )
                ),
                0,
                self._a,
                self._b,
                8,
            )
            self._b += 8
            jakobyArrayCopy(
                self.f(
                    self.d(
                        self.d(
                            self.d(self.d(0, 0, var4.h()), 13, var4.c()), 4, var4.i()
                        ),
                        12,
                        var4.d(),
                    )
                ),
                0,
                self._a,
                self._b,
                4,
            )
            self._b += 4

            for cosi in var1:
                var8 = self.c(0, 0, cosi.e())
                var10 = self._a
                var10[self._b] = var8
                self._b += 1
                jakobyArrayCopy(
                    self.f(
                        self.d(
                            self.d(
                                self.d(self.d(0, 0, cosi.c()), 13, cosi.b()),
                                4,
                                cosi.d(),
                            ),
                            12,
                            cosi.a(),
                        )
                    ),
                    0,
                    self._a,
                    self._b,
                    4,
                )
                self._b += 4

            self.o(var6)
            self.j(188 - self._b)
            self.p()

    def a(self, var1: int, var2: int) -> None:
        self._d.c().g(var1)
        self._d.c().f(0)
        self._d.c().h(0)
        self._d.c().i(var2)

    def b(self, var1: int, codec: Codec) -> None:
        var3 = Becko()
        var3.g(var1)
        var3.f(0)
        var1 = codec.value
        if var1 != 1:
            if var1 != 2:
                if var1 != 3:
                    if var1 == 4:
                        var3.h(192)
                        var3.e(17)
                        var3.i(144)
                        self._e = len(self._d.c().c())
                else:
                    var3.h(192)
                    var3.e(17)
                    var3.i(15)
                    self._e = len(self._d.c().c())

            else:
                var3.h(224)
                var3.e(18)
                var3.i(27)
                self._f = len(self._d.c().c())

        else:
            var3.h(224)
            var3.e(18)
            var3.i(36)
            self._f = len(self._d.c().c())

        var1 = self._d.c().d()
        self._d.c().h(var1 + 1)
        self._d.c().c().append(var3)

    def k(self, var1: list, var2: int, var3: int) -> list:
        if var1 is None:
            return None
        else:
            var5 = var1
            if len(var1) > var2:
                var5 = jakobyUtilArraysCopyOf(var1, var2)

            self.n()
            self.a(66, 1)
            var6 = Codec.MUXTS_CODEC_PCMA
            self.b(68, var6)
            self.r(var5, var3, var6)
            return self.l()

    def l(self) -> list:
        var1 = len(self._c)
        if var1 > 0:
            var2 = [x for x in self._c]
            return var2
        else:
            return None

    def m(self) -> None:
        self._c = None

    def n(self) -> None:
        self._b = 0
        # self._c = [0 for i in range(512000)]
        self._c = []
        self._d.e(0)
        self._d.d(0)
        self._d.g(0)
        self._d.f(Cecko())
        self._g = 0
        self._h = []
        self._i = []

    def r(self, var1: list, var2: int, var4: Codec) -> None:
        if var1 is not None:
            var5 = 0
            var6 = 0
            var7 = 0
            var8 = 0
            var9 = 0
            var14 = False

            outer_break = False
            while True:  # label81
                while True:  # label92
                    var5 = len(var1)
                    if Codec.MUXTS_CODEC_HEVC != var4 and Codec.MUXTS_CODEC_AVC != var4:
                        if Codec.MUXTS_CODEC_AAC == var4:
                            var6 = self._e
                            var7 = var5 + 8
                            break

                        if Codec.MUXTS_CODEC_PCMA != var4:
                            var14 = False
                            var7 = 0
                            var8 = 0
                            var9 = 0
                            outer_break = True
                            break

                        var7 = self._e
                    else:
                        var7 = self._f

                    var6 = var7
                    var7 = var5
                    break

                if outer_break:
                    break

                var9 = var6
                var8 = var7
                var7 = 0
                var14 = False
                break

            while True:
                while var5 > 0:
                    var13 = Becko()
                    var15 = 0
                    if self._g == 0:
                        var12 = g.Ecko()
                        var12.e(self._d.c().e())
                        var12.d(self._d.c().b())
                        var12.f(255)
                        self._h.append(var12)

                        for var15 in range(self._d.c().d()):
                            var16 = g.Hacko()
                            var13 = self._d.c().c()[var15]
                            var16.g(var13.b())
                            var16.j(var13.d())
                            var16.f(0)
                            var16.h(255)
                            var16.i(255)
                            self._i.append(var16)

                    if self._g % 64 == 0:
                        self.s(self._d.b(), 1, 1, self._d.a())
                        self.t(self._h, True)
                        var15 = self.h(self._d.a())
                        self._d.d(var15)
                        self._g += 1
                        self.s(self._d.c().b(), 1, 1, self._d.c().a())
                        self.v(self._i, self._d.c().e(), True)
                        var15 = self.h(self._d.c().a())
                        self._d.c().f(var15)
                        self._g += 1
                    else:
                        var17 = None
                        if var14:
                            var17 = self._d.c().c()[var9]
                            var20 = []
                            if var5 >= 184:
                                self.s(var17.b(), 0, 1, var17.a())
                                var20 = self._a
                                var15 = self._b
                                jakobyArrayCopy(var1, var7, var20, var15, 188 - var15)
                                var15 = self._b
                                var5 -= 188 - var15
                                self._b = var15 + (188 - var15)
                                var7 += 188 - var15
                            else:
                                self.s(var17.b(), 0, 3, var17.a())
                                self.q(188 - var5 - 5, False, g.Cecko())
                                var20 = self._a
                                var15 = self._b
                                jakobyArrayCopy(var1, var7, var20, var15, 188 - var15)
                                var15 = self._b
                                var7 += 188 - var15
                                var5 -= 188 - var15
                                self._b = var15 + (188 - var15)

                            var17.f(self.h(var17.a()))
                            self._g += 1
                            self.p()
                        else:
                            var17 = self._d.c().c()[var9]
                            self.s(var17.b(), 1, 3, var17.a())
                            var18 = g.Cecko()
                            var18.d(var2)
                            var18.f(255)
                            var18.e(0)
                            if self._j:
                                self.q(7, True, var18)
                            elif (
                                Codec.MUXTS_CODEC_AAC != var4
                                and Codec.MUXTS_CODEC_PCMA != var4
                            ):
                                self.q(7, True, var18)
                            else:
                                self.q(1, False, var18)

                            var19 = g.Icko()
                            var19.h(2)
                            var19.l(255)
                            var19.m(255)
                            var19.n(255)
                            var19.i(jakoby_int_overflow(var2 >> 30))
                            var19.j(jakoby_int_overflow(var2 >> 15))
                            var19.k(var2)
                            var13 = self._d.c().c()[var9]
                            self.u(var8, var13.c(), 2, 5, var19)
                            var13.f(self.h(var13.a()))
                            self._g += 1
                            var6 = self._b
                            if var5 >= 188 - var6:
                                jakobyArrayCopy(var1, var7, self._a, var6, 188 - var6)
                                var6 = self._b
                                var7 += 188 - var6
                                var5 -= 188 - var6
                                self._b = var6 + (188 - var6)

                            self.p()
                            var14 = True

                return
